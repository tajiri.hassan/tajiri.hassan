<h1 align="right">👋<img width=15%" src="https://github.com/alansmathew/alansmathew/raw/master/lang.gif" alt="lang image here"/><b>Je m'appelle hassan ☺️</b></h1>

### Je suis Développeur web junior, passionné de technologie et j'aime apprendre de nouvelles choses...


## 🧔 Mon Profile en quelques lignes:
En reconversion professionnelle, je suis actuellement à la recherche d'un contrat de stage pour une période de deux mois, du 2 mai au 13 juillet 2022, en tant que Développeur web.
Je suis passionné par le monde du web et j'aimerais m'orienter définitivement vers ce domaine afin d'y travailler sur le long terme. Je suis surtout intéressé par le développement informatique, et plus particulièrement par le développement web.
Grâce aux compétences que j'ai acquises au cours de mon parcours professionnel et de mon expérience personnelle, j'ai développé des capacités de gestion et d'analyse, ainsi que la capacité de travailler en équipe et de manière autonome. Je suis créative, bon communicateur et j'ai l'esprit d'équipe, et je suis déterminé à mettre mes compétences au service d'un projet réel.

## 🤓 Un peu plus sur moi:

 <b>👨 Débutant avec esprit  (**ouvert a l'apprentissaget**)</b><br>
 <b>📈 Je suis en train d'apprendre PHP, Javascript et Scss.</b><br>
 <b>💻Je travaille actuellement sur la création de mon blog a fin de partager mes sources et mes connaissances en programmation.</b><br>
 <b>🌱 J'aime faire Jardinier, coder, jouer de laguitare, et faire de velo pendant mon temps libre.</b><br>
 <b>🙌 J'aime partager  tout ce que j'ai appris.</b><br>
 <b>❔  Interrogez-moi sur le marketing numérique et le support informatique.</b><br>

## 🎯 Objectifs de 2022:

<b>mon but est de réussir mes études de développement web et d'obtenir mon diplôme, terminer mon projet de site dynamique avec un blog personnel. et enfin obtenir mon premier emploi en tant que développeur web dans une équipe cool prête à soutenir et partager ses connaissances avec un développeur web junior.</b>

## 📚 Langues & Frameworks:

![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap) 
![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript)  ![MySQL](https://img.shields.io/badge/-MySQL-black?style=flat-square&logo=mysql)

### 🔧 Outils:

- ![Git](https://img.shields.io/badge/-Git-black?style=flat-square&logo=git)

## 💡 Projects:

- Disponible bientôt...

## 🔗  Trouvez-moi ailleurs

[![SOF Badge](https://img.shields.io/static/v1?label=StackOverflow&message=Overflow&link=https://stackoverflow.com/users/16041672/h-t/)](https://stackoverflow.com/users/16041672/h-t/)
[![Linkedin Badge](https://img.shields.io/badge/hassan-tajiri-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/hassan-tajiri/)](https://www.linkedin.com/in/hassan-tajiri/)
[![Gmail Badge](https://img.shields.io/badge/-mailto:tajiri.hassan@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:tajiri.hassan@gmail.com)](mailto:tajiri.hassan@gmail.com)
[![Website Badge](https://img.shields.io/static/v1?label=Web&message=Portfolio&color=Bluelink=https://hass-1.github.io/portfolio)](https://hass-1.github.io/portfolio) (In progress...)

## 📈 Aperçu de mes activités sur Github:
[![My GitHub stats](https://github-readme-stats.vercel.app/api?username=hass-1&layout=compact&title_color=007bff&text_color=e7e7e7&icon_color=007bff&bg_color=171c28)](https://github.com/hass-1/github-readme-stats)
 ![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=hass-1&layout=compact&title_color=007bff&text_color=e7e7e7&icon_color=007bff&bg_color=171c28) 

    8 consecutive days with commits

# Vous aimez mon profile et vous voulez créer le vôtre ?
 C'est très simple. Gitlab a récemment ajouté une nouvelle fonctionnalité appelée Profile Readmes.  Pour qu'elle fonctionne, procédez comme suit :
 <ol>
<li>Créez un dépôt Gitlab spécial avec votre nom d'utilisateur comme nom de dépôt. (Mon nom d'utilisateur est hassan.tajiri, donc mon dépôt de readme de profil a le nom hassan.tajiri). </li>
<li>Ajoutez un README.md à ce dépôt.</li>
<li>Mettez du contenu sympa sur vous (ou tout ce que vous voulez) dans le fichier README.md.</li>
</ol>
Et c'est tout. Le README.md du dépôt readme de votre profil sera affiché sur votre page de profil.
vous pouvez utiliser le language Markdown, ou tout simplement de HTML.<br>

**Si vous avez besoin d'inspiration, vous pouvez me laisser un mot et je vais vous aider.**


